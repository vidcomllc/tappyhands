//
//  EndViewController.swift
//  Tappy Hands
//
//  Created by Cory Hall on 1/11/18.
//  Copyright © 2018 Cory Hall. All rights reserved.
//

import UIKit
import Social
import MessageUI

class EndViewController: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    
    
    
    
    
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    
    @IBOutlet var scoreLabel: UILabel!
    
    var scoreData:String!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        label1.layer.cornerRadius = 5.0
        label2.layer.cornerRadius = 5.0
        button1.layer.cornerRadius = 5.0
        button2.layer.cornerRadius = 5.0
        button3.layer.cornerRadius = 5.0
        button4.layer.cornerRadius = 5.0
        
        scoreLabel.text = scoreData
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func restartGame(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.presentingViewController?.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func shareTwitter(_ sender: Any) {
            
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
                
                
                let twitter:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                
                twitter.setInitialText("My final score was: \(scoreLabel.text!)")
                
                self.present(twitter, animated: true, completion: nil)
                
                
            } else {
                let alert = UIAlertController(title: "Accounts", message: "Please log into your Twitter account within the settings", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
    }
    
    @IBAction func shareEmail(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail:MFMailComposeViewController = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(nil)
            mail.setSubject("I bet you can't beat me!")
            mail.setMessageBody("My final score was: \(scoreLabel.text!)", isHTML: false)
            
            self.present(mail, animated: true, completion: nil)
            
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please log into your account", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func shareSMS(_ sender: Any) {
        
        if MFMessageComposeViewController.canSendText() {
            let msg:MFMessageComposeViewController = MFMessageComposeViewController()
            msg.messageComposeDelegate = self
            
            msg.recipients = nil
            msg.body = "My final score was: \(scoreLabel.text!)"
            
            self.present(msg, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "This device can not send SMS messages", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    

}
